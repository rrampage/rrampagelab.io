---
title: "Memory Management in Hotspot JVM"
subtitle: ""
tags: [java,jvm,memory,gc,hotspot]
---

Oracle's JVM troubleshooting course gives an overview on memory management, Hotspot's Garbage collection options, various memory errors and how to troubleshoot them. This is material from Week 1.

## Memory Management in Hotspot JVM

The JVM provides automatic memory management to free the programmer from manually managing memory. The unused objects are freed using garbage collection.

### Automatic Memory Management

- New Objects allocated on heap memory
- **Root set** - pointers to external memory like static variables, threads, JNI refs and internal JVM structures
- Objects directly reachable from root set must be kept in heap
- Objects reachable from reachable objects must also be in heap
- Unreachable objects (Garbage) are removed
- Compaction - Reachable objects are moved to contiguous space in heap


### Generational GC and Memory Spaces in Hotspot

- Memory space divided into various generations
- Separate pools hold objects of diff age ranges
- Hypothesis:
    - Most objects die young
    - Few references from older to younger objects
- 2 generations
    - young : small and collected frequently (minor collection). Objects which survive a threshold number of GCs move to old generation.
    - old : large, collected infrequently (major collection = Full GC)
- Prior to JDK 8, there was also a permanent generation which was for storing class representations and metadata, interned strings and class statics. Replaced by meta-space in JDK 8 and later. 
- Meta-space is allocated in native memory
 	- Managed through JVM options **MetaspaceSize** for initial size and **MaxMetaspaceSize** for max. 
 	- If **UseCompressedClassPointers** is enabled, 2 areas of mem are used for classes and its metadata - metaspace and compressed class space.
 	- 64 bit class pointers represented with 32 bit offsets. Class metadata is referenced by 32 bit offsets stored in compressed class space
 	- By default, compressed class space is 1 GB
	 - **MaxMetaspaceSize** sets upper limit on commited size of both these spaces
- Code cache is used to store compiled code generated by JIT, allocated out of native mem and managed by Code Cache Sweeper


### Garbage Collectors in Hotspot JVM

- Young generation collection
	 - Serial - Stop-the-world (STW), copying collector, single threaded
 	- ParNew - STW, copying collector, multiple GC threads
 	- Parallel Scavenge - STW, copying collector, multiple GC threads
- Old generation collection
 	- Serial Old - Stop-the-world (STW), mark-sweep-compact collector, single threaded
	- CMS - Mostly concurrent, low pause
	 - Parallel Old - compacting collector, multiple GC threads
- G1 : designed for large heaps and offers predictable short pauses. 
	 - Has different memory layout for generations
	 - Same collector for all generations

#### GC options
- UseSerialGC : Serial + SerialOld
- UseParNewGC : ParNew + SerialOld . In JDK 9, uses CMS for old gen
- UseConcMarkSweepGC : ParNew + CMS + SerialOld 
	 - CMS used most of time to collect old generation. SerialOld used when concurrent mode failure occurs. 
	 - CMS performs most work in concurrent with application threads.
 	- No heap compaction leads to fragmentation. Has floating garbage and requires larger heap sizes.
 	- Free space maintained as linked lists. Allocation expensive compared to bump-the-pointer allocations.
 	- Additional overhead on young collections
	 - Deprecated in JDK 9
- UseParallelGC : Parallel Scavenge + Parallel Old. 
 	- Maximizes throughput. 
 	- Default GC till JDK 9
- UseG1GC - G1 for both generations
 	- Server style GC for multi-core machines with large memory
 	- Low GC pauses with high probability while trying for high throughput
 	- Compacting collector. Low pauses without fragmentation
 	- Better GC ergonomics. Parallel threads and some tasks are concurrent with application threads
 	- Since JDK 7u4 and default in JDK 9

[GC Tuning Guide](https://docs.oracle.com/javase/9/gctuning/toc.htm)

#### How young generation is collected or minor GC takes place:

1. When Eden space in young gen is full, reachable objects are marked and moved to the ToSurvivorSpace
2. Objects in FromSurvivor space that are reachable are moved to ToSurvivorSpace
3. Objects in FromSurvivor space that have crossed the threshold are promoted to the old generation
4. Eden becomes empty and is ready for new allocations
5. To and From Survivor Spaces are switched

#### Mark-Sweep-Compact collector (Serial Old)
- Mark phase : marks all live objects
- Sweep phase : sweeps over heap identifying garbage
- Slide phase : GC performs a sliding compaction by sliding live objects to the start of the heap


