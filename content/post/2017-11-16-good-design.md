---
title: "Good Design"
subtitle: ""
tags: [diary,design]
---

> Perfection is reached not when there is no longer anything to add, but when there is no longer anything to take away - A Saint Exupery

> Perfection must be reached by degrees; she requires the slow hand of time - Voltaire

> The unavoidable price of reliability is simplicity - C Hoare

Good Program Design

 - Keep basic interfaces stable
 - Don't hide power
 - Divide and conquer
 - Do one thing at a time and do it well (KISS)
 - Handle normal and worst cases separately
 - Make actions atomic or restartable
 