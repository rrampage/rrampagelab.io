---
title: Features of a CMS
date: 2017-10-05
tags: [diary,design]
---

### Features for a good CMS:
- A highly available database cluster
- A straightforward way to manage business object data life cycles
- Transparently secure user authentication & authorisation
- HTML templating
- User & system error handling
- A responsive static file server
- Clear definition of request routes with paths & methods & handlers
- All forms inescapably protected from Cross Site Request Forgery attacks
- Texts and labels in multiple languages
- Sending email
- HTTPS with certificate generation
- HTTP2 would be welcome
- A short-cycled build system
- An automated script to build computing environments
- A no-brains way to scale out by adding computing resources
- A declarative rate limiting capability, protecting from robots & Denial of Service attacks