---
title: Some Reflections on Programming Careers
date: 2017-10-06
tags: [diary,career]
---

### Some quotes:

Conway’s law
> The organization of a team will eventually be reflected in the architecture of the product that team builds.

From [@alicegoldfuss](https://twitter.com/alicegoldfuss/status/913927942094200832)
> Tech careers are very much about how you present yourself and your successes. People gloss over the details, because that's how you get hired as a rock star.

>**Switching to a new career path involves:**
>
> - find materials 
> - study on my own time, for hours 
> - have faith that I was smart enough to do this 
> - change jobs 
> - have already-valuable skills 
> - negotiate a change 
> - have supportive team mates
> - not give up
 
 
### The Joel Test

-  Do you use source control?
-  Can you make a build in one step?
-  Do you make daily builds?
-  Do you have a bug database?
-  Do you fix bugs before writing new code?
-  Do you have an up-to-date schedule?
-  Do you have a spec?
-  Do programmers have quiet working conditions?
-  Do you use the best tools money can buy?
-  Do you have testers?
-  Do new candidates write code during their interview?
-  Do you do hallway usability testing?