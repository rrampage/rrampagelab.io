---
title: About
subtitle: Nothing to add really...
comments: false
---

#### About me
Till I think of something interesting, see footer for relevant info

#### About site

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/) and [hugo](https://gohugo.io) and uses the `beautifulhugo` theme.
